import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2DatetimePickerModule } from 'ng2-datetime-picker';
import { AppComponent }  from './app.component';
import { HomeComponent }  from './app.home';
import { AddComponent }  from './app.add';

const appRoutes: Routes = [  
  { path: '', component: HomeComponent },
  { path: 'add-item', component: AddComponent }
] ;

@NgModule({
  id: module.id,
  imports:      [ BrowserModule, RouterModule.forRoot(appRoutes), Ng2DatetimePickerModule],
  declarations: [ AppComponent, HomeComponent, AddComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
