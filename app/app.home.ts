import { Component, OnInit } from '@angular/core';


declare var moment: any;
moment['locale']('en-ca'); //e.g. fr-ca


import { Ng2Datetime  } from 'ng2-datetime-picker';

@Component({
    selector: 'my-app',
    template: `<h1> {{title}} </h1>
            <ul>
                <li *ngFor="let item of cartList"> {{item.name}} </li>
            </ul>
            <input [(ngModel)]="myDate" ng2-datetime-picker date-only="false" date-format="DD-MM-YYYY" />
            <a class="btn btn-primary" (click)="getDate(myDate)">Get Date</a>
              `,
})
export class HomeComponent implements OnInit { 
    title : 'Home page';
    cartList: { id: number, name: string }[] = [
        { "id": 0, "name": "Available" },
        { "id": 1, "name": "Ready" },
        { "id": 2, "name": "Started" }
    ];
    getDate (dt: Date) {
        console.log(dt);
        alert(dt);
    }
    ngOnInit () {

    }
}
