import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent }  from './app.component';

@Component({
    selector: 'my-app',
    template: `<h1> {{title}} </h1>`,
})
export class AddComponent { title = 'Add item here' }
