import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl:'/app/app.component.html',
})

export class AppComponent  { projectName = 'Angular2 POC App'; }
